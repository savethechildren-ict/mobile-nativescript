# Safely: Save The Children - ICT
This is the mobile cross-platform codebase for Save The Children's ICT initiative.

This application is built using Nativescript (Angular2), with a Firebase backend.
It uses Telerik UI Nativescript Pro (binaries included in the codebase).
Google Maps is used for the map functions.

---
# Hardware Prerequisites

### Windows
Only Genymotion has hard requirements for this application's tech stack.
Check the req's [here](https://docs.genymotion.com/Content/01_Get_Started/Installation.htm).

### OS X
XCode 8.3 can only be installed on OSX 10.12 (Sierra) or later.
Check your Macbook model for compatibility.

---
# Software Prerequisites
The following software needs to be installed on any dev machine:

### OS X
1. Start with a fresh install of OS X 10.13 (High Sierra)
- From the App Store, install: `XCode`
- Install [Homebrew](http://brew.sh/)
- Install Homebrew Cask: `brew cask`
- Install wget: `brew install wget`
- Install Git: `brew install git`
    - open a new terminal to refresh your ENV_VARS, then setup your Git global vars:
        - `git config --global user.name "<user name in Gitlab>"`
        - `git config --global user.email "<email in Gitlab>"`
- Install ZSH: `brew install zsh`
    - open a new terminal, and confirm zsh's location: `which zsh`
        - it should be: `/usr/local/bin/zsh`
    - set Homebrew's zsh as your user's default shell: `sudo dscl . -create /Users/$USER UserShell /usr/local/bin/zsh`
    - open a new terminal, and confirm that zsh is the current shell: `which $SHELL`
        - You should be asked if you would like to write `.zshrc`; opt to create it, with any defaults if offered
    - if issues arise, read: [Use Homebrew zsh Instead of the OS X Default](https://rick.cogley.info/post/use-homebrew-zsh-instead-of-the-osx-default/)
- Install OhMyZSH:
    - follow install instructions via wget in: [robbyrussell/oh-my-zsh](https://github.com/robbyrussell/oh-my-zsh)
    - use plugins;
        - I personally add:
            - `z`: already included in OhMyZSH, just enable in .zshrc's `plugins` line
            - [zsh-autosuggestions](https://github.com/zsh-users/zsh-autosuggestions): follow the instructions for OhMyZSH plugin
- Install [ZSH-Spaceship](https://github.com/denysdovhan/spaceship-zsh-theme): follow the OhMyZSH plugin instructions
    - manually edit your `.zshrc` if instructed
- Install NVM:
    - `brew install nvm`; don't forget to add double-check the insructions that follow; check your `.zshrc`
    - `nvm ls-remote --lts`: list latest LTS
    - `nvm install <version>`
    - `nvm ls`: check if `node` and `npm` are using the correct versions
- Install RBenv: follow instructions in [rbenv/rbenv](https://github.com/rbenv/rbenv#homebrew-on-macos)
    - NOTE: **NEVER** run `sudo gem install xxx`; this will use the root account's Ruby version; to have your user's own Ruby installation is cleaner
    - make sure to follow the instructions, especially double-checking your `.zshrc`
    - `rbenv install --available`: list available versions
    - `rbenv install 2.3.5`
    - `rbenv rehash`: run this whenever you install/set a new Ruby version / gem
    - `rbenv global 2.3.5`: double-check after with: `ruby --version`
    - `gem install xcodeproj`: once it finishes, follow up with: `rbenv rehash`
    - `gem install cocoapods`: once it finishes, follow up with: `rbenv rehash`
    - make sure the gems and XCode are properly setup:
        - try running: `xcodebuild`
            - it should say that there are too few parameters (invalid usage, but okay installation)
                - if it errors out, try fixing with: `sudo xcode-select --reset`
        - make sure that Cocoapods is setup: run: `pod setup`
            - this is a long download, with failure possible within multiple tries
            - search the net; try using SSH instead of HTTPS
- Install Jabba: follow instructions in [shyiko/jabba](https://github.com/shyiko/jabba)
    - `jabba ls-remote`: check available versions
    - `jabba install zulu@~1.8.144`: install latest 1.8 JDK
    - `jabba use zulu@~1.8.144`: use 1.8
    - `echo "zulu@~1.8.144" > ~/.jabbarc`: setup .jabbarc
    - add to .zshrc: `jabba use`
    - open a new terminal and check if Java is recognized
- Install [Android Studio](https://developer.android.com/studio/index.html)
    - on top of default SDK packages, add:
        - API 26 stack: SDK Platform 26, Build Tools 26
        - Google Play Services
    - add ANDROID_HOME to your .zshrc: `export ANDROID_HOME=/Users/***/Library/Android/sdk`
- Install Virtualbox: `brew cask install virtualbox`
- [Genymotion](https://docs.genymotion.com/Content/01_Get_Started/Installation.htm)
    - open `Genymotion` > `Settings` > `ADB`, use `Custom Android SDK`, and put your `ANDROID_HOME` path
    - add any device that you wish to emulate
- Install Nativescript
    - `npm i -g nativescript`
    - `tns doctor`

### Windows
1. Install [Chocolatey](https://chocolatey.org/install)
    - open Powershell with Admin rights
    - enable Remote Script Install: `Set-ExecutionPolicy RemoteSigned`
    - run the install script (details in website)
- Install all tools:
    - `choco install -y git`
    - `choco install -y cmder`
        - always use Admin Rights, with profile: `bash::bash as Admin`
    - `choco install -y visualstudiocode`
        - after installation, add the [Nativescript VS Code Extension](https://www.nativescript.org/nativescript-for-visual-studio-code)
    - `choco install -y jdk8`
    - `choco install -y android-sdk`
        - run the SDK Manager: `android`
            - might not work in CMDer; try in CMD instead
        - execute SDK Manager as Administrator, and make sure that the ff. packages are installed:
            - `Tools/Android SDK Tools`
            - `Tools/Android SDK Platform Tools`
            - `Tools/Android SDK Build-tools/*`
            - (Major API version, API 21 to highest)`/SDK Platform`
            - `Extras/*` (install all applicable)
- [Genymotion](https://docs.genymotion.com/Content/01_Get_Started/Installation.htm)
    - open `Genymotion` > `Settings` > `ADB`, use `Custom Android SDK`, and put your `ANDROID_HOME` path
    - add any device that you wish to emulate
    - for each device, install/update Google Play (for Google Maps SDK): follow the guide [here](https://github.com/codepath/android_guides/wiki/Genymotion-2.0-Emulators-with-Google-Play-support#setup-google-play-services)
- [NVM for Windows](https://github.com/coreybutler/nvm-windows)
    - `choco install -y nvm`
    - afterwards, install the **latest** Node LTS version:
        - `nvm list available`; take note of the latest LTS version; ex: 6.9.5
        - `nvm install 6.9.5 <arch>` (default: 64-bit; example for 32-bit: nvm install 6.9.5 32)
        - `nvm list` (Node 6.9.5 must be available)
        - `nvm use <version>`
    - check by executing: `npm version` & `node --version`
    - install Nativescript npm module: `npm i -g nativescript`
        - check by executing: `tns doctor`
- [Generate your SSH keys](https://help.github.com/articles/generating-an-ssh-key/)
- Register your SSH key to our Gitlab repo: savethechildren-ict/mobile-nativescript
- Setup your local repo:
    - Configure your git settings:
        - `git config --global user.name "<user name in Gitlab>"`
        - `git config --global user.email "<email in Gitlab>"`
    - Clone the repo on your local machine:
        - `git clone git@gitlab.com:savethechildren-ict/mobile-nativescript`

Your system should now be ready to build the application.

---
# Dev Workflow
1. Clone the project: `git clone git@gitlab.com:savethechildren-ict/mobile-nativescript.git`
- Open the mobile-nativescript folder in VS Code (if debugging, open with Admin rights)
- Open the mobile-nativescript folder in the CLI with Admin rights: `cd mobile-nativescript`
- If on Windows: edit *nativescript-plugin-firebase*
    - if building on a Windows machine, edit `firebase.nativescript.json` and set: `"using_ios": false`
- Install Node dependencies and Nativescript plugins: `tns install`
        - you may run the nativescript-plugin-firebase setup script anytime by:
            - `cd node_modules/nativescript-plugin-firebase`
            - `npm run setup`
- Build the application: `tns build android --clean`
- Setup *nativescript-directions*
    - after each `tns install` (every time node_modules is recreated), you need to set the target platform (default is iOS):
        - open: `node_modules/nativescript-directions/index.d.ts`
        - on line 5, change to your target platform
- Make sure to that your local build is pointing to the correct environment (See `Environments` section below)
- Start an Android emulator device, or connect your physical Android device, and check if connected: `tns device`
    - Run the application on the emulator: `tns run android --emulator`
    - Run the application on the device: `tns run android --device <device id>`
- To debug / inspect the view tree, run: `tns debug android --start --emulator`
- To use webpack / create a bundled .apk (smaller/faster):
    -  Delete build folders
    - `npm install`
    - `npm run start-android-bundle`
        - (As of NS 3.1, applicable only for OS X / Linux) To package a snapshot as well (even faster startup): `npm run start-android-bundle --snapshot`

### Additional Notes:
- If there are no changes to any Node modules / Nativescript plugins, you can livesync changes from code to the running app: `tns livesync android --emulator --watch`
- If a new plugin was introduced (or you're encountering weird build errors) and you wish to rebuild the everything from scratch:
    1. Delete these folders:
        - hooks
        - node_modules
        - lib
        - platforms
    - Redo `Dev Workflow` steps starting from Step 4.


---
# Environments
For the backend APIs, there will be 3 environments: DEV, UAT and PROD
- `DEV`: for local testing
- `UAT`: for client testing
- `PROD`: for users

|  	    | Firebase 	        | Google Maps 	    | Facebook 	    |
|------	|---------------	|---------------	|---------------	|
| DEV 	| [safely-sample](https://console.firebase.google.com/project/safely-sample/overview) 	| [safely-sample](https://console.cloud.google.com/apis/dashboard?project=safely-sample) 	| [safely-sample](https://developers.facebook.com/apps/686728491484788/dashboard/)  |
| UAT 	| *to follow* 	    | *to follow* 	    | *to follow* 	    |
| PROD 	| *to follow* 	    | *to follow* 	    | *to follow* 	    |

### Switching Environments
*to follow*

---
# Collaboration Workflow
1. Every proposed change will have to start with an issue. Raise a [Gitlab issue](https://gitlab.com/savethechildren-ict/mobile-nativescript/issues), and include all relevant details.
- Once an issue is created, click on its detail view and click: `New Branch`. This will create a feature branch for the sole development of the feature being worked on.
- You may now start dev work on your local repo:
    1. Synchronize your local repo:
        - VS Code: click the **refresh icon** besides the current branch name at the bottom-left
        - CLI: `git fetch` -> `git pull`
    - Checkout the feature branch:
        - VS Code: click the current branch name, and in the Command Pallete, enter: `git branch <feature branch name>`
        - CLI: `git checkout <feature branch name>`
    - Do your changes / commits to the feature branch
        - VS Code: select the Git view, and add your commited files to the the staging area. Commit once done.
        - CLI: `git add <filename>` or `git add *` -> `git commit -m <message>`
    - Occasionally poll the remote repository for any changes in the master branch (upstream change); if there were upstream changes in master, pull them and merge them into the feature branch (there are many ways to do this, but merging the updated master into your local feature branch is the easiest):
        - to check if the remote master was updated:
            - checkout master
            - In the CLI, check the master branch's status: `git status`. If your local master is out-of-date, you will be notified that you are several commmits behind
            - synchronize to pull changes from the remote repo (see Step 1 above)
        - if master was changed:
            - checkout your feature branch
            - In the CLI, merge the updated local master changes into your feature branch: `git merge master`
            - If there are merge conflicts, adjust accordingly:
                - for each file difference flagged, edit the file and clean the changes
                - once finished, add the file back into the staging area: `git add <filename>`
        - Once master is merged into the feature branch, the feature branch is now updated with the latest changes from master.
        - Back in your local repository, while still checked-out on the feature branch, push the updated local feature branch to its remote:
            - VS Code: click the **refresh icon**
            - CLI: `git push`
- Once all feature changes are done, sync/push a final commit and create a Merge Request to merge your feature branch into master.
    1. In Gitlab, go to: `Repository` -> `Branches` and for your feature branch, click: `Merge Request`
    - In the Merge Request Form, the defaults should be okay, but make sure that:
        - the Source branch is: `the feature branch`
        - the Target branch is: `master`
        - In the description, tag the issue number/link (using the syntax below) to have the merge automatically close the issue being addressed:
            - `Closes #xxx and https://gitlab.com/<username>/<projectname>/issues/<xxx>` (this closes 2 issues: one referenced by number and another by issue link)
            - see a more detailed explanation [here](https://about.gitlab.com/2016/10/25/gitlab-workflow-an-overview/#merge-request)
        - You may opt to select if the branch should be deleted once the merge request is accepted
    - Complete the Merge request
- If the feature branch changes are okay, the repo owner will accept the request and the changes will be merged to master.

---
# Recommendations

### Optimization
- Currently, all processing (data retrieval, filtering and sorting) happens at client-side (in the mobile device).
- Due to the current small size of the entire testing center data, no issues should be noticed
- For the price of renting a dedicated server, running these data processes server-side can lighten the load on the mobile device, possibly leading to a faster and less battery-consuming app experience.
- Estimates:
    - [f1-micro (shared CPU, 0.6 GB RAM): $4.68 per month](https://cloud.google.com/products/calculator/#id=35902726-e6ca-4908-98fb-4ea084c0d7c5)
    - [g1-small (shared CPU, 1.7 GB RAM): $15.33 per month](https://cloud.google.com/products/calculator/#id=ae02d4e4-5a52-449e-991e-720cbe8def59)
    - [n1-standard-1 (dedicated CPU, 3.75 GB RAM): $28.19 per month](https://cloud.google.com/products/calculator/#id=5f0146e4-3f19-4b62-ae6b-7ed05a58f2f7)

### Posts
- One of the previous requirements (that did not make it into the final build) was to include a view in the mobile app for viewing posts and articles, with Wordpress as the desired platform for managing the posts
- If this feature will be targeted for a future build, it is important that the Wordpress setup be exposed to an HTTP REST API for the app to be able to retrieve the post content and metadata
- the suggested plugin is: [WP REST API v2](http://v2.wp-api.org/)

---
# Guides
If you are unaware of certain tech, you can find basic information/commands in the guides below:
- Basics
    - [Git](http://rogerdudler.github.io/git-guide/)
        - useful commands:
            - prune all stale remotes: `git remote prune origin`
            - stash current modified/untracked files: `git stash`
            - recover stash: `git stash apply --index`
    - [Basic Git Workflow](https://about.gitlab.com/2016/10/25/gitlab-workflow-an-overview/)
    - [Markdown](http://commonmark.org/help/)
    - [TypeScript: Deep Dive](https://www.gitbook.com/book/basarat/typescript)
- Angular
    - [Component Interaction](https://angular.io/docs/ts/latest/cookbook/component-communication.html)
    - [Providers](https://www.sitepoint.com/angular-2-components-providers-classes-factories-values/)
- Nativescript
    - [Nativescript CLI](https://github.com/NativeScript/nativescript-cli)
    - [Nativescript API](http://docs.nativescript.org/api-reference/globals)
    - [Nativescript Plugin: Google Maps SDK](https://github.com/dapriett/nativescript-google-maps-sdk)
    - [Nativescript Plugin: Firebase](https://github.com/EddyVerbruggen/nativescript-plugin-firebase)
    - Nativescript UI / UX:
        - from Nativescript Docs:
            - [Working with Images](https://docs.nativescript.org/ui/images#load-images-from-a-resource)
            - [Supporting Multiple Screens](http://docs.nativescript.org/ui/supporting-multiple-screens)
        - from Nativescript Blog:
            - [Supporting Multiple Screens](https://www.nativescript.org/blog/supporting-multiple-screen-resolutions-in-your-nativescript-app)
        - Tools:
            - [Nativescript Image Builder](http://nsimage.nativescript.rocks/)
    - Nativescript Angular
        - [Lifecycle Hook diffs bet. Web And {N}](https://github.com/NativeScript/nativescript-angular/issues/374)
- Publishing
    - Android
        - [Android] (https://docs.nativescript.org/publishing/publishing-android-apps)
    - iOS
        - [Process Checklist] (https://docs.nativescript.org/publishing/publishing-ios-apps)
        - [Provisioning Profile Guide](https://wiki.appcelerator.org/display/guides2/Deploying+to+iOS+devices)
