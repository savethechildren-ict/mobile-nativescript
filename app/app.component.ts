// Core
import { Component, OnInit } from "@angular/core";
import { Page } from "ui/page";

// Google Maps SDK
import * as platform from "platform";
declare var GMSServices: any;

// Firebase
import firebase = require("nativescript-plugin-firebase");

@Component({
    selector: "stc-app",
    moduleId: module.id,
    templateUrl: "app.component.html"
})
export class AppComponent implements OnInit {

    constructor(
        private page: Page) {
    }
    
    ngOnInit(): void {
        // View
        this.page.actionBarHidden = true;

        // Google Maps SDK
        if (platform.isIOS) { 
            GMSServices.provideAPIKey("AIzaSyAyQEo1Jlsg0qWs11umRMlxLu0LHjiMoQA");
        }

        // Firebase
        firebase.init({
                // Optionally pass in properties for database, authentication and cloud messaging,
                // see their respective docs.

                // Realtime Database
                // set true in PROD 
                persist: false
            }).then(
                (instance) => {
                    console.log("firebase.init done");
                },
                (error) => {
                    console.log("firebase.init error: " + error);
                }
            );
    }
}
