export class Clinic {
    key?: string;

    institution?: string;

    typeHygieneClinic?: boolean;
    typeTreatmentHub?: boolean;
    typeCondomStop?: boolean;

    poc?: ClinicPerson;

    address?: string;
    city?: string;
    province?: string;
    region?: string;

    lat?: number;
    long?: number;

    services?: Array<ClinicService>;
    labtests?: Array<ClinicService>;
    facilities?: Array<ClinicFacility>;

    counselors?: Array<ClinicPerson>;
    keys?: Array<ClinicPerson>;
    doctors?: Array<ClinicPerson>;
    educators?: Array<ClinicPerson>;

    schedule?: {
        sunday: ClinicSchedule;
        monday: ClinicSchedule;
        tuesday: ClinicSchedule;
        wednesday: ClinicSchedule;
        thursday: ClinicSchedule;
        friday: ClinicSchedule;
        saturday: ClinicSchedule;
    };

    email?: string;
    numbers?: Array<ClinicNumber>;

    [propName: string]: any;  
}

export class ClinicService {
    name: string;
    price: number;
    details: string;
}

export class ClinicFacility {
    name: string;
}

export class ClinicPerson {
    name: string;
    designation: string;
}

export class ClinicSchedule {
    open: boolean;
    start: string;
    end: string;
}

export class ClinicNumber {
    main: boolean;
    type: string;
    number: string;
    ext: string;
    details: string;  
}