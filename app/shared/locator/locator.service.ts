// Core
import { Injectable } from "@angular/core";

// Models
import { Clinic } from "./clinic";

// Firebase
import firebase = require("nativescript-plugin-firebase");

@Injectable()
export class LocatorService {

    getClinics(): Promise<Clinic[]> {
        return firebase.query(
            (result) => {
                // console.log("LocatorService.getTestingCenters result: " + JSON.stringify(result));
            },
            "/v1/clinics",
            {
                singleEvent: true,
                orderBy: {
                    type: firebase.QueryOrderByType.CHILD,
                    value: 'id'
                }
            }).then((result) => {
                return Promise.resolve(result.value);
            });
    }
}
