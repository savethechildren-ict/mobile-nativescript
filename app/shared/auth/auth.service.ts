// Core
import { Injectable } from "@angular/core";

// Firebase
import firebase = require("nativescript-plugin-firebase");

@Injectable()
export class AuthService {

    getCurrentUser(): Promise<firebase.User> {
        return firebase.getCurrentUser()
            .then(
                function (result) {
                    console.log(JSON.stringify(result));
                    return Promise.resolve(result);
                },
                function (errorMessage) {
                    console.log(errorMessage);
                    return Promise.reject(errorMessage);
                }
            );
    }

    login(): Promise<firebase.User> {
        return firebase.login({
                type: firebase.LoginType.FACEBOOK,
                facebookOptions: {
                    scope: ['public_profile', 'email'] // optional: defaults to ['public_profile', 'email']
                }
            }).then(
                function (result) {
                    JSON.stringify(result);
                    return Promise.resolve(result);
                },
                function (errorMessage) {
                    console.log(errorMessage);
                    return Promise.reject(errorMessage);
                }
          );
    }

    logout(): Promise<any> {
        return firebase.logout();
    }
}
