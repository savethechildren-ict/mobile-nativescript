// Core
import { Injectable } from "@angular/core";

// Dependencies
import firebase = require("nativescript-plugin-firebase");

@Injectable()
export class FirebaseUtilService {
    
    /** Convert an array of objects with pushId keys to a simple object array. the pushId becomes a property of each individual object. */
    convertToArrayWithPushId(keyedArray) {
        let outputArray = new Array<any>();
        for (let key in keyedArray) {
            let item = keyedArray[key];
            item.pushId = key;
            outputArray.push(item);
        }
        return outputArray;
    }
}