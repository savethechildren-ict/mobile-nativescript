// Core
import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptModule } from "nativescript-angular/nativescript.module";
import { AppRoutingModule, navigatableComponents, entryComponents } from "./app.routing";

// Components
import { AppComponent } from "./app.component";

// Services
import { LocatorService } from "./shared/locator/locator.service";
import { AuthService } from "./shared/auth/auth.service";
import { FirebaseUtilService } from "./shared/util/firebase.util.service"; 
import { ModalDialogService } from "nativescript-angular/modal-dialog";

// Font Icons
import { TNSFontIconModule } from 'nativescript-ngx-fonticon';

//Slides
import { SlidesModule } from 'nativescript-ngx-slides';
import { ItemService } from "./shared/items/item.service";
import { LearnDetailComponent } from "./pages/learn-detail/learn-detail.component";


@NgModule({
    declarations: [
        AppComponent,
        ...navigatableComponents
    ],
    providers: [
        LocatorService,
        AuthService,
        FirebaseUtilService,
        ModalDialogService,
        ItemService
    ],
    imports: [
        NativeScriptModule,
        AppRoutingModule,
        SlidesModule,
		TNSFontIconModule.forRoot({
			'fa': './assets/font-awesome.css'
		})
    ],
    entryComponents: [
        ...entryComponents
    ],
    bootstrap: [
        AppComponent
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})
export class AppModule { }
