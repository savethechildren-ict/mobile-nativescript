// Core
import { Component, OnInit, OnDestroy } from "@angular/core";
import { ModalDialogParams } from "nativescript-angular/modal-dialog";
import scrollViewModule = require("ui/scroll-view");

// Services
import { FirebaseUtilService } from "../../shared/util/firebase.util.service";

// Models
import { Clinic, ClinicService, ClinicSchedule, ClinicNumber } from "../../shared/locator/clinic";

// Directions
import { Directions } from "nativescript-directions";
import geolocation = require("nativescript-geolocation");

// Call 
import * as phone from "nativescript-phone";

// PhotoViewer
const PhotoViewer = require("nativescript-photoviewer");
const frameModule = require("ui/frame");
const imageModule = require("ui/image");

@Component({
    moduleId: module.id,
    selector: "stc-locator-detail",
    templateUrl: "locator-detail.component.html",
    styleUrls: ["locator-detail-common.css", "locator-detail.css"]
})
export class LocatorDetailComponent implements OnInit, OnDestroy {
    // State
    currentItem: Clinic;
    currentLoc: geolocation.Location; 
    currentImages: Array<any>;

    // Directions
    directions: Directions;

    constructor(
        private firebaseUtilService: FirebaseUtilService,
        private modalParams: ModalDialogParams) {
        this.currentItem = modalParams.context.testingCenter;
        this.currentLoc = modalParams.context.currentLoc;
    }

    ngOnInit() {
        console.log("LocatorDetailComponent.ngOnInit() STARTED!");

        // State init
        this.currentImages = this.firebaseUtilService.convertToArrayWithPushId(this.currentItem.images);
        //console.log(JSON.stringify(this.currentItem.images));
        // console.log(this.currentImages.length);

        // Directions init
        this.directions = new Directions();
    }

    ngOnDestroy() {
        // console.log("LocatorDetailComponent.ngOnDestroy() STARTED!");
    }
    
    isItemTesting(): boolean {
        return this.currentItem.typeHygieneClinic;
    }

    isItemTreatment(): boolean {
        return this.currentItem.typeTreatmentHub;
    }
    isItemCondom(): boolean {
        return this.currentItem.typeCondomStop
    }

    getStoreStatus(): string {
        let result = "";

        // Add moment.js and time support
        //console.log();
        result = "Open NOW";

        return result;
    }

    closeLocatorDetail() {
        this.modalParams.closeCallback();
    }

    openDirections(lat: number, long: number) {
        this.directions.navigate({
            from: {
                lat: this.currentLoc.latitude,
                lng: this.currentLoc.longitude
            },
            to: {
                lat: lat,
                lng: long
            }
        }).then(() => {
            console.log("LocatorDetailComponent.openDirections() SUCCESS!");
        }, (error) => {
            console.log("LocatorDetailComponent.openDirections() FAILED: " + JSON.stringify(error));
        });
    }

    makeCall(phoneNumber: string) {
        phone.dial(phoneNumber, true);
    }

    sendSMS(phoneNumber: string) {
        phone.sms([phoneNumber],"My Message")
            .then((args) => {
                /// args.reponse: "success", "cancelled", "failed"
                console.log("LocatorDetailComponent.sendSMS() SUCCESS! status: " + JSON.stringify(args));
            }).catch((error) => {
                console.log("LocatorDetailComponent.sendSMS() FAILED: " + JSON.stringify(error));
            });
    }

    // PhotoViewer
    openGallery() {
        const images = this.currentImages.map(function(item) {
            return item.url;
        });

        (new PhotoViewer()).showViewer(images);
    }
}