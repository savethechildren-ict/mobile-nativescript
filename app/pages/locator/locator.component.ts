// Core
import { Component, OnInit, ViewChild, ChangeDetectorRef, ElementRef, ViewContainerRef } from "@angular/core";
import { Image } from "ui/image";
import * as platform from "platform";

// Components
import { LocatorDetailComponent } from "../locator-detail/locator-detail.component"

// Services
import { LocatorService } from "../../shared/locator/locator.service";

// Models
import { Clinic } from "../../shared/locator/clinic";

// Modal and Dialogs
import * as dialogs from "ui/dialogs";
import { ModalDialogService, ModalDialogOptions } from "nativescript-angular/modal-dialog";

// Font Icons
import { TNSFontIconService } from 'nativescript-ngx-fonticon';

// Loading Icon
const LoadingIndicator = require("nativescript-loading-indicator").LoadingIndicator;

// Permissions
const permissions = require("nativescript-permissions");

// Exit
import { exit } from 'nativescript-exit';

// Search
import { SearchBar } from "ui/search-bar";
import { Observable } from "rxjs/Rx";
import "rxjs/add/observable/fromEvent";
import "rxjs/add/operator/debounce";

// SegmentedBar
import * as ImageSource from "image-source";
import fs = require("file-system");

// Geolocation
import * as geolocation from "nativescript-geolocation";
import { Accuracy } from "ui/enums"; // used to describe at what accuracy the location should be get
const geolocationOptions = {
    desiredAccuracy: Accuracy.high, 
    maximumAge: 5000, 
    timeout: 10000 
}

// Google Maps SDK
import { MapView, Position, Marker, Style } from "nativescript-google-maps-sdk";
import { mapStyle } from "../../shared/locator/mapstyle";
import { registerElement } from "nativescript-angular/element-registry";
registerElement("MapView", () => require("nativescript-google-maps-sdk").MapView);

// Google Maps SDK - Android
declare var com:any;
declare var android:any;

// Google Maps SDK - iOS
declare var GMSMapStyle: any;
declare var GMSCameraPosition: any;
declare var CATransaction: any;
declare var kCATransactionAnimationDuration: any;
declare var NSNumber: any;

export enum CenterType {
    All = 0,
    TestingCenter,
    TreatmentCenter,
    CondomStop
}

@Component({
    selector: "stc-locator",
    moduleId: module.id,
    templateUrl: "locator.component.html",
    styleUrls: ["locator-common.css", "locator.css"]
})
export class LocatorComponent implements OnInit {
    // View
    isMapActive: boolean;

    // State
    isInitialLoad: boolean;

    // SegmentedBar
    selectedCenter: CenterType;

    // Google Maps SDK
    mapView: MapView;
    markerIconTesting: Image;
    markerIconTreatment: Image;
    markerIconCondom: Image;
    ZOOM_LEVEL = 14;
    ZOOM_DURATION = 1000;

    // ListView
    items: Array<Clinic> = [];

    searchItems: Array<Clinic> = [];
    allItems: Array<Clinic> = [];

    searchTestingItems: Array<Clinic> = [];      // Hygiene Clinic
    allTestingItems: Array<Clinic> = [];         // Hygiene Clinic

    searchTreatmentItems: Array<Clinic> = [];    // Treatment Hub
    allTreatmentItems: Array<Clinic> = [];       // Treatment Hub

    searchCondomItems: Array<Clinic> = [];       // Condom Stop
    allCondomItems: Array<Clinic> = [];          // Condom Stop

    // Search 
    @ViewChild("searchBar") searchBar: ElementRef;
    search: SearchBar;

    // Map Detail
    currentItem: Clinic = {
        institution: "Hello!",
        distance: 0
    };

    // loader
    loader = new LoadingIndicator();

    constructor(
        private locatorService: LocatorService,
        private modalDialogService: ModalDialogService,
        private viewContainerRef: ViewContainerRef,
        private changeDetectorRef: ChangeDetectorRef,
        private fonticon: TNSFontIconService
    ) {
    }
    
    ngOnInit(): void {
        // View init
        this.isMapActive = true;

        // State init
        this.isInitialLoad = true;

        // Search init
        this.search = <SearchBar>this.searchBar.nativeElement;
        this.search.isEnabled = false;

        // Search: set clearEvent
        this.search.on(SearchBar.clearEvent, (args) => {
            console.log("Search Bar cleared!");
            this.loader.show();
            this.resetCurrentItems()
                .then(() => {
                    this.changeDetectorRef.detectChanges();
                    this.loader.hide();
                }).catch((error) => {
                    this.changeDetectorRef.detectChanges();
                    this.loader.hide();
                });
        });

        // Search: observable init
        Observable.fromEvent(this.search, "textChange")
            .debounceTime(1000)
            .subscribe((event: any) => {
                console.log("Search is starting: " + this.search.text); 
                
                // let clearEvent handle empty search strings
                if (this.search.text == "") {
                    console.log("Search string is empty!");
                } else {
                    this.loader.show();
                    this.searchItemsByTerm(this.search.text)
                        .then(() => {
                            this.changeDetectorRef.detectChanges();
                            this.loader.hide();
                        }).catch((error) => {
                            this.changeDetectorRef.detectChanges();
                            this.loader.hide();
                        });
                    if (this.isMapActive) {
                        this.isMapActive = false;
                    }
                }
            });

        // SegmentedBar init
        this.selectedCenter = CenterType.All;

        // Image init
        this.markerIconTesting = new Image();
        this.markerIconTesting.imageSource = ImageSource.fromFile(fs.path.join(fs.knownFolders.currentApp().path, "pages", "locator", "images", "01-testingcenters.png"));

        this.markerIconTreatment = new Image();
        this.markerIconTreatment.imageSource = ImageSource.fromFile(fs.path.join(fs.knownFolders.currentApp().path, "pages", "locator", "images", "02-treatmentcenters.png"));

        this.markerIconCondom = new Image();
        this.markerIconCondom.imageSource = ImageSource.fromFile(fs.path.join(fs.knownFolders.currentApp().path, "pages", "locator", "images", "03-condomstops.png"));
        
    }

    // Map events
    onMapReady(event) {
        console.log("onMapReady() is starting!");
        this.loader.show();

        // Map Init
        this.mapView = event.object;
        this.mapView.zoom = this.ZOOM_LEVEL;

        // first, ask location permission
        this.ensureLocationIsEnabled()
            .catch(result => {
                // Location request was denied; exit the application
                console.log("geolocation.enableLocationRequest() REJECTED");
                exit();
            })
            .then(result => {
                // setup: map settings, permission-dependent, platform-specific

                // setup: android-specific
                if (platform.isAndroid) {
                    // set: map style
                    this.mapView.gMap.setMapStyle(
                        new com.google.android.gms.maps.model.MapStyleOptions(JSON.stringify(mapStyle)));

                    // show: location button
                    let uiSettings = this.mapView.gMap.getUiSettings();
                    uiSettings.setMyLocationButtonEnabled(true);
                    this.mapView.gMap.setMyLocationEnabled(true);

                    // search: remove focus
                    // if (this.search.android) {
                    this.search.android.clearFocus();
                    this.search.android.setFocusable(false);
                    // }
                }
                
                // setup: ios-specific
                if (platform.isIOS) {
                    // set: map style
                    this.mapView.nativeView.mapStyle = GMSMapStyle.styleWithJSONStringError(JSON.stringify(mapStyle));

                    // show: location button
                    this.mapView.gMap.myLocationEnabled = true;
                    this.mapView.gMap.settings.myLocationButton = true;

                    // TODO: remove focus from search bar on ios
                }

                // setup: get testing centers
                return this.locatorService.getClinics();
            })
            .then((clinics) => {
                let result = this.objectToArray(clinics);

                if (this.isInitialLoad) {
                    // setup: memoize all master lists
                    this.allItems = new Array();
                    this.allTestingItems = new Array();
                    this.allTreatmentItems = new Array();
                    this.allCondomItems = new Array();
    
                    for (var i = 0; i < result.length; i++) {
                        //this.allItems = result;
                        this.allItems.push(result[i]);

                        // Insert into corresponding filtered master list type
                        if (result[i].typeHygieneClinic) {
                            this.allTestingItems.push(result[i]);
                        };
                        if (result[i].typeTreatmentHub) {
                            this.allTreatmentItems.push(result[i]);
                        };
                        if (result[i].typeCondomStop) {
                            this.allCondomItems.push(result[i]);
                        };
    
                        // Initialize markers (that have coordinates)
                        if (result[i].lat != null) {
                            let marker = new Marker();
                            marker.position = Position.positionFromLatLng(result[i].lat, result[i].long);
                            marker.title = result[i].institution;
                            marker.userData = result[i].key;
                            marker.snippet = result[i].city;
    
                            // Add the marker to the master lists
                            if (result[i].typeHygieneClinic) {
                                marker.icon = this.markerIconTesting;
                                this.allTestingItems[this.allTestingItems.length - 1].marker = marker
                            };
                            if (result[i].typeTreatmentHub) {
                                marker.icon = this.markerIconTreatment;
                                this.allTreatmentItems[this.allTreatmentItems.length - 1].marker = marker
                            };
                            if (result[i].typeCondomStop) {
                                marker.icon = this.markerIconCondom;
                                this.allCondomItems[this.allCondomItems.length - 1].marker = marker
                            };

                            this.allItems[i].marker = marker;
                        }
                    }
                    
                    // reset current items, add map markers, then center
                    return this.resetCurrentItems();
                } else {
                    // current items already existing;
                    // add map markers, then center
                    this.updateMapMarkers(this.mapView, this.items);
                    this.addMarkerAndZoom(this.mapView, this.currentItem);

                    return Promise.resolve();
                }
            })
            .then(() => {
                // hide loader
                this.loader.hide();
            })
            .catch(error => {
                this.loader.hide();
                console.log("LocatorComponent.onMapReady() FAILED!: " + JSON.stringify(error));
            }); 
                
    };

    onMarkerSelect(userData) {

        // Assign selected marker to current item
        // TODO: leverage logic with: onItemTap()
        this.currentItem = this.allItems.find((item) => {
            if (item.key === userData) {
                return true;
            }
        });
        this.goToMapView();
    }

    /**
     * Clean mapView of all markers, and populate with markers from a Clinic array.
     * @param mapView 
     * @param sourceItems 
     */
    private updateMapMarkers(mapView: MapView, sourceItems: Array<Clinic>) {
        mapView.removeAllMarkers();

        sourceItems.forEach((item) => {
            if (item.lat != null) {
                let marker = this.createMarkerFromItem(item);
                mapView.addMarker(marker);
            };
        });
    }

    /**
     * Create a marker from a Clinic item.
     * @param item 
     */
    private createMarkerFromItem(item: Clinic): Marker {
        let marker = new Marker();
        marker.position = Position.positionFromLatLng(item.lat, item.long);
        marker.title = item.institution;
        marker.userData = item.key;
        marker.snippet = item.city;

        // TODO: put selectedCenter in params
        switch(+this.selectedCenter) {
            case CenterType.All:
                // choose the icon from the last type seen
                if (item.typeHygieneClinic) {
                    marker.icon = this.markerIconTesting;
                };
                if (item.typeTreatmentHub) {
                    marker.icon = this.markerIconTreatment;
                };
                if (item.typeCondomStop) {
                    marker.icon = this.markerIconCondom;
                };
                break;
            case CenterType.TestingCenter:
                marker.icon = this.markerIconTesting;
                break;
            case CenterType.TreatmentCenter:
                marker.icon = this.markerIconTreatment;
                break;
            case CenterType.CondomStop:
                marker.icon = this.markerIconCondom;
                break;
        }

        return marker;
    }

    /**
     * UI method. Set the clicked item from the search list as the current item.
     * @param id
     */
    onItemTap(id) {
        if (this.items[id].lat != null) {
            this.currentItem = this.items[id];
            this.goToMapView();
        } else {
            // TODO: Create a warning UI label; for now, log in console; is this even reachable?
            console.log("onItemTap(): This marker has no lat/long values!");
        }
    }

    /**
     * UI method. Add marker & zoom to current item on the map.
     */
    onCurrentItemTap() {
        if (this.currentItem.lat != null) {
            this.addMarkerAndZoom(this.mapView, this.currentItem);
        } else {
            // TODO: Create a warning UI label; for now, log in console; is this even reachable?
            console.log("onCurrentItemTap(): This marker has no lat/long values!");
        }
    }

    /**
     * Create marker from Testing Center, show info window then zoom
     * @param mapView 
     * @param Clinic 
     */
    addMarkerAndZoom(mapView: MapView, clinic: Clinic) {
        console.log("addMarkerAndZoom: starting...");

        let marker = this.createMarkerFromItem(clinic);
        mapView.addMarker(marker);
        marker.showInfoWindow();

        this.zoomTo(mapView, clinic.lat, clinic.long);
    }

    /**
     * Zoom mapView camera to the given location
     * @param mapView
     * @param lat 
     * @param long 
     */
    private zoomTo(mapView: MapView, lat: number, long: number) {
        console.log("Zooming in on: lat: "  + lat + "; long: " + long);

        this.getCurrentLocation()
            .then(result => {
                if (platform.isAndroid) {
                    /*
                    mapView.gMap.animateCamera(
                        new com.google.android.gms.maps.CameraUpdateFactory.newLatLngZoom(
                            new com.google.android.gms.maps.model.LatLng(lat, long)
                            ,this.ZOOM_LEVEL)
                        ,this.ZOOM_DURATION
                        ,null);
                    */
        
                    let builder = new com.google.android.gms.maps.model.LatLngBounds.Builder();
                    builder.include(new com.google.android.gms.maps.model.LatLng(this.currentItem.lat, this.currentItem.long));
                    builder.include(new com.google.android.gms.maps.model.LatLng(result.latitude, result.longitude));
                    let bounds = builder.build();
                    mapView.gMap.animateCamera(
                        new com.google.android.gms.maps.CameraUpdateFactory.newLatLngBounds(bounds, 300)
                    );                
                }
                
                if (platform.isIOS) {
                    CATransaction.begin();
                    CATransaction.setValueForKey(NSNumber.numberWithFloat(this.ZOOM_DURATION / 1000), kCATransactionAnimationDuration);
                    mapView.nativeView.animateToCameraPosition(GMSCameraPosition.cameraWithLatitudeLongitudeZoomBearingViewingAngle(
                        lat,
                        long,
                        this.ZOOM_LEVEL,
                        0,
                        0
                    ));
                    CATransaction.commit();
                }
            })
    }

    /**
     * UI method. Change currently selected Center Type Filter
     * @param centerType 
     */
    changeCenterType(centerType: CenterType) {
        // If button is already active, deactivate and remove all filters
        if (this.selectedCenter == centerType) {
            this.selectedCenter = CenterType.All;

            this.items = this.searchItems;
        } else {
            this.selectedCenter = centerType;

            switch (this.selectedCenter) {
                case CenterType.TestingCenter:
                    console.log("seg item 1 clicked: " + this.isMapActive + "; " + this.searchTestingItems.length);
                    this.items = this.searchTestingItems;
                    break;
                case CenterType.TreatmentCenter:
                    console.log("seg item 2 clicked: " + this.isMapActive + "; " + this.searchTreatmentItems.length);
                    this.items = this.searchTreatmentItems;
                    break;
                case CenterType.CondomStop:
                    console.log("seg item 3 clicked: " + this.isMapActive + "; " + this.searchCondomItems.length);
                    this.items = this.searchCondomItems;
                    break;
            }
        }
        
        if (this.isMapActive) {
            // If current view is map, update markers
            this.updateMapMarkers(this.mapView, this.items);
        } else {
            // If current view is search, run the search again for the selected type
            this.searchItemsByTerm(this.search.text);
        }

        this.changeDetectorRef.detectChanges();
    }

    /**
     * Reset current item array, add map markers, then center to current item. 
     * If on Initial Load, current item is set to the nearest Testing Center.
     */
    resetCurrentItems() {
        console.log("LocatorComponent.resetCurrentItems() BEGIN");

        this.searchItems = this.allItems;
        this.searchTestingItems = this.allTestingItems;
        this.searchTreatmentItems = this.allTreatmentItems;
        this.searchCondomItems =this.allCondomItems;

        return Promise.resolve()
            .then((result) => {
                // reset master list, depending on currently selected Center Type
                switch(this.selectedCenter) {
                    case CenterType.All:
                        return this.getCentersSortedByDistance(this.searchItems);
                    case CenterType.TestingCenter:
                        return this.getCentersSortedByDistance(this.searchTestingItems);
                    case CenterType.TreatmentCenter:
                        return this.getCentersSortedByDistance(this.searchTreatmentItems);
                    case CenterType.CondomStop:
                        return this.getCentersSortedByDistance(this.searchCondomItems);
                }
            })
            .then(result => {
                // set active item array
                this.items = result;
                this.changeDetectorRef.detectChanges();

                // On Initial Load, set nearest SHC as the current item
                console.log("Updating current item...");
                if (this.isInitialLoad) {
                    this.currentItem = this.items[0];
                    this.isInitialLoad = false;
                }

                // update map markers and zoom
                this.updateMapMarkers(this.mapView, this.items);
                this.addMarkerAndZoom(this.mapView, this.currentItem);
            })
    }

    // TODO: show in UI if nothing was found
    /**
     * Update the active item array according to the: 
     * 1) center list, of the currently selected center type; and
     * 2) filtered by the current search term
     * @param term 
     */
    searchItemsByTerm(term: string): Promise<void> {
        console.log("LocatorComponent.searchItemsByTerm() BEGIN! term: " + term);

        this.searchItems = this.filterItems(this.allItems, term);
        this.searchTestingItems = this.filterItems(this.allTestingItems, term);
        this.searchTreatmentItems = this.filterItems(this.allTreatmentItems, term);
        this.searchCondomItems = this.filterItems(this.allCondomItems, term);

        return Promise.resolve()
            .then(result => {
                switch(this.selectedCenter) {
                    case CenterType.All:
                        return this.getCentersSortedByDistance(this.searchItems);
                    case CenterType.TestingCenter:
                        return this.getCentersSortedByDistance(this.searchTestingItems);
                    case CenterType.TreatmentCenter:
                        return this.getCentersSortedByDistance(this.searchTreatmentItems);
                    case CenterType.CondomStop:
                        return this.getCentersSortedByDistance(this.searchCondomItems);
                }
            })
            .then(result => {
                // set current items
                this.items = result;
                this.updateMapMarkers(this.mapView, this.items);

                this.changeDetectorRef.detectChanges();
                console.log("LocatorComponent.searchItemsByTerm() SUCCESS!");
            })
            .catch((error) => {
                console.log("LocatorComponent.searchItemsByTerm() FAILED: " + JSON.stringify(error));
            });
    }

    /**
     * Filters a list of Testing Centers based on a given search term.
     * @param sourceItems 
     * @param term 
     */
    private filterItems(
            sourceItems: Array<Clinic>, 
            term: string) {
        let filteredItems = new Array<Clinic>();
        filteredItems = sourceItems.filter((clinic) => {
                return (clinic.city.toLowerCase().indexOf(term.toLowerCase())        !== -1  ||
                        clinic.institution.toLowerCase().indexOf(term.toLowerCase()) !== -1  ||
                        clinic.province.toLowerCase().indexOf(term.toLowerCase())    !== -1  ||
                        clinic.region.toLowerCase().indexOf(term.toLowerCase())      !== -1  
                );
            }); 
        return filteredItems;
    }

    /**
     * Returns an array of Testing Centers sorted by the distance of current user from the item's location
     * @param items 
     */
    private getCentersSortedByDistance(
            items: Array<Clinic>): Promise<Array<Clinic>> {

        return this.getCurrentLocation()
            .then(result => {
                let newItems = items;

                for (var i = 0; i < newItems.length; i++) {
                    let testLoc: geolocation.Location = {
                        latitude: newItems[i].lat,
                        longitude: newItems[i].long,
                        altitude: 0,
                        horizontalAccuracy: 0,
                        verticalAccuracy: 0,
                        speed: 0,
                        direction: 0,
                        timestamp: null,
                        //android: null,
                        //ios: null
                    };
                    testLoc.latitude = newItems[i].lat;
                    testLoc.longitude = newItems[i].long;
                    newItems[i].distance = geolocation.distance(result, testLoc) / 1000;
                }

                return Promise.resolve(newItems.sort((a, b) => a.distance - b.distance));
            });
    }

    /**
     * Get user's current location. Also checks if location is enabled. Will reject if fail.
     */
    private getCurrentLocation(): Promise<geolocation.Location> {
        console.log("LocatorComponent.getCurrentLocation() BEGIN!");

        return this.ensureLocationIsEnabled()
            .then(result => {
                return geolocation.getCurrentLocation(geolocationOptions)
            })
            .then((result) => {
                // console.log('Are we getting here?');
                return Promise.resolve(result);
            })
    }

    /**
     * Check if location can be retrieved. Resolves if location can be obtained, rejects if fail.
     */
    private ensureLocationIsEnabled(): Promise<void> {
        if (platform.isIOS) {
            // If iOS, resolve
            return Promise.resolve();
        } else if (platform.isAndroid) {
            // If Android

            // first, ask for app's location permissions (specific to Android 6+ / API 23+)
            return permissions.requestPermission(android.Manifest.permission.ACCESS_COARSE_LOCATION)
                .then(result => {
                    // second, ask for location services
                    if (geolocation.isEnabled()) {
                        console.log("Geolocation IS enabled!");

                        return Promise.resolve();
                    } else {
                        console.log("Geolocation IS NOT enabled!");

                        return dialogs.confirm("Safely needs your current location. Please enable your phone's Location service.")
                            .then(result => {
                                if (!result) {
                                    return Promise.reject(null);
                                };
                            })
                            .then(result => {
                                return geolocation.enableLocationRequest(true);
                            });
                    }  
                })
        }
    }

    /**
     * UI method. Open the Testing Center Detail Modal.
     */
    openLocatorDetail() {
        this.loader.show();
        return this.getCurrentLocation()
            .then(result => {
                let options: ModalDialogOptions = {
                    viewContainerRef: this.viewContainerRef,
                    context: {
                        testingCenter: this.currentItem,
                        currentLoc: result
                    },
                    fullscreen: true
                };
                this.loader.hide();
                return this.modalDialogService.showModal(LocatorDetailComponent, options);
            });
    }

    /**
     * UI method. Go to the Map View.
     */
    goToMapView() {
        this.isMapActive = true;
    }

    /**
     * UI method. Go to the Search View.
     */
    goToSearchView() {
        this.isMapActive = false;
    }

    /**
     * UI method. Get CenterType.All.
     */
    getCenterTypeAll() {
        return CenterType.All;
    }

    /**
     * UI method. Get CenterType.Clinic.
     */
    getCenterTypeTesting() {
        return CenterType.TestingCenter;
    }

    /**
     * UI method. Get CenterType.TreatmentCenter.
     */
    getCenterTypeTreatment() {
        return CenterType.TreatmentCenter;
    }

    /**
     * UI method. Get CenterType.CondomStop.
     */
    getCenterTypeCondom() {
        return CenterType.CondomStop;
    }

    objectToArray(source: Object) {
        let result = [];
        for (let i of Object.keys(source)) {
            let obj = source[i];
            obj.key = i;

            result.push(obj);
        }
        return result;
    }
}
