// Core
import { Component, OnInit } from "@angular/core";

// View
import { SelectedIndexChangedEventData } from 'ui/tab-view';

// Font Icons
import { TNSFontIconService } from 'nativescript-ngx-fonticon';

@Component({
    selector: "stc-home",
    moduleId: module.id,
    templateUrl: "home.component.html",
    styleUrls: ["home-common.css", "home.css"]
})
export class HomeComponent implements OnInit {
    // View
    tabSelectedIndex: number;

    constructor(
        private fonticon: TNSFontIconService
    ) {
        this.tabSelectedIndex = 0;
    }

    ngOnInit(): void {
    }

    indexChanged(event: SelectedIndexChangedEventData): void {
        if (!event) {
            alert("HomeComponent.indexChanged: event is null!");
        } else {
            let title:String;
            switch (event.newIndex) {
                case 0:
                    //title = "Locator"
                    //this.profile.raiseAlert();
                    //this.homeService.tabSwitch(0);
                    break;
                case 1:
                    //this.locator.refresh();
                    //this.homeService.tabSwitch(1);
                    break;
                case 2:
                    //title = "Webview"
                    //this.webview.raiseAlert();
                    //this.homeService.tabSwitch(2);
                    break;
                default:
                    alert("tabSelectedIndex is out-of-bounds!");
                    break;
            }
        }
    }
}
