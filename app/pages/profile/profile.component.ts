// Core
import { Component, OnInit } from "@angular/core";

// Dialog
import * as dialogs from "ui/dialogs";

// Services
import { AuthService } from "../../shared/auth/auth.service";

// Firebase
import firebase = require("nativescript-plugin-firebase");

// Font Icons
// import { TNSFontIconService } from 'nativescript-ngx-fonticon';

@Component({
    selector: "stc-profile",
    moduleId: module.id,
    templateUrl: "profile.component.html",
    styleUrls: ["profile-common.css", "profile.css"]
})
export class ProfileComponent implements OnInit {
    user: firebase.User;

    constructor(
        // private fonticon: TNSFontIconService
        private authService: AuthService
    ) {
        this.user = null;
    }
    
    ngOnInit(): void {
        // try to get the current user
        this.authService.getCurrentUser()
            .then((result) => {
                this.user = result;
            })
            .catch((error) => {
                console.log('No user is currently logged in!');
            })
    }

    login() {
        console.log("Facebook Login!");
        this.authService.login()
            .then((result) => {
                this.user = result;
            })
            .catch((error) => {
                console.log('User login failed!');
            });
    }

    logout() {
        this.authService.logout()
            .then((result) => {
                this.user = null;
            })
            .catch((error) => {
                console.log('User logout failed!');
            });
    }

    openAboutUs() {
        dialogs.alert({
            title: "#SafelyPH",
            message: "#SafelyPH is a mobile app that provides HIV information, directs users to the nearest social hygiene clinic, and helps users be safe and healthy in a fun and interactive way. \n \n This project is an initiative of Save The Children in partnership with The Red Whistle under the Global Fund to fight AIDS, Tuberculosis, and Malaria.",
            okButtonText: "OK"
        }).then(() => {
            console.log("Dialog closed!");
        });
    }

}
