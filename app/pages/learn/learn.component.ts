// Core
import { Component, OnInit, ViewChild, ChangeDetectorRef, ElementRef, ViewContainerRef } from "@angular/core";

// Components
import { LearnDetailComponent } from "../learn-detail/learn-detail.component"

// Detail
import { ModalDialogService, ModalDialogOptions } from "nativescript-angular/modal-dialog";

//Website
import { openUrl } from "tns-core-modules/utils/utils";

// Font Icons
import { TNSFontIconService } from 'nativescript-ngx-fonticon';

@Component({
    selector: "stc-learn",
    moduleId: module.id,
    templateUrl: "learn.component.html",
    styleUrls: ["learn-common.css", "learn.css"]
})
export class LearnComponent implements OnInit {

    constructor(
        private modalDialogService: ModalDialogService,
        private viewContainerRef: ViewContainerRef,
        private fonticon: TNSFontIconService
    ) {
    }

    ngOnInit(): void {
    }

    // nativescript-3-webpack: turn off temporarily
    openLearnDetail() {
        let options: ModalDialogOptions = {
            viewContainerRef: this.viewContainerRef,
            context: {
            //    testingCenter: null,
            //    currentLoc: null
            },
            fullscreen: true
        };
        
        this.modalDialogService.showModal(LearnDetailComponent, options);
            //.then((dateresult: Date) => {
                // << (hide)
            //});
        // << returning-result
    }

    launchSafelyWeb() {
       openUrl("https://safelyph.com");
       console.log("Launch #Safely PH Web!");
    }
}
