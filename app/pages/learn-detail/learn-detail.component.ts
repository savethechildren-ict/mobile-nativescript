// Core
import { Component, OnInit, OnDestroy } from "@angular/core";
import { ModalDialogParams } from "nativescript-angular/modal-dialog";
import scrollViewModule = require("ui/scroll-view");

//Slides
import { Item } from "../../shared/items/item";
import { ItemService } from "../../shared/items/item.service";

@Component({
    moduleId: module.id,
    selector: "stc-learn-detail",
    templateUrl: "learn-detail.component.html",
    styleUrls: ["learn-detail-common.css", "learn-detail.css"]
})
export class LearnDetailComponent implements OnInit, OnDestroy {

    items: Item[];

    constructor(
        private modalParams: ModalDialogParams,
        private itemService: ItemService) {
    }

    ngOnInit() {
        this.items = this.itemService.getItems();
        console.log("LearnDetailComponent.ngOnInit() STARTED!");
    }
    
    onChanged(index) {
        console.log('Slides changed! Current slide index:', index);
    }

    onFinished() {
        console.log('Slides finished!');
    }

    ngOnDestroy() {
        console.log("LearnDetailComponent.ngOnDestroy() STARTED!");
    }

    //closeLearnDetail() {
    //    this.modalParams.closeCallback();
    //}

}