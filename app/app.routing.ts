// Core
import { NgModule } from "@angular/core";
import { NativeScriptRouterModule } from "nativescript-angular/router";
import { Routes } from "@angular/router";

// Components
import { HomeComponent } from "./pages/home/home.component";
import { ProfileComponent } from "./pages/profile/profile.component";
import { LocatorComponent } from "./pages/locator/locator.component";
import { LocatorDetailComponent } from "./pages/locator-detail/locator-detail.component";
import { LearnComponent } from "./pages/learn/learn.component";
import { LearnDetailComponent } from "./pages/learn-detail/learn-detail.component";

export const navigatableComponents = [
    HomeComponent,
        ProfileComponent,
        LocatorComponent,
            LocatorDetailComponent,
        LearnComponent,
            LearnDetailComponent
];

export const entryComponents = [
    LocatorDetailComponent,
    LearnDetailComponent
];

const routes: Routes = [
    { path: "", redirectTo: "/home", pathMatch: 'full' },
    { path: "home", component: HomeComponent }
];

@NgModule({
    imports: [NativeScriptRouterModule.forRoot(routes)],
    exports: [NativeScriptRouterModule]
})
export class AppRoutingModule { }